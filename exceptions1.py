class MyException(Exception):
    pass


try:
    a = input("input data: ")
    if not a.isdigit():
        raise MyException(f"Ваш ввод повинен бути валідним числом - {a}", a)
    a = float(a)
    print(10 / a)
except ZeroDivisionError:
    print("ділити на ноль - не вмію!")
except MyException as err:
    print(err.args[0])
    print(err.args[1].upper())
else:
    print("виключень не було!")
finally:
    print("спрацює будь-коли")




