import sys

if not <condition>:
    raise AssertionError("message")

assert <condition>, "message"


def func(input_list):
    """
    input: Dict[str, List]
    len(List) == 3
    List[int]

    output:
    Dict[str, List]
    """
    for i in input_list:
        if not isinstance(i, (str,)):
            return None


print(sys.platform)
assert ('win32' in sys.platform), "This code runs on Linux only."

try:
    assert func({1: []}) == None, "hhjj"
    assert func({'fff': [12, 34}) == {}
except AssertionError:
    pass




